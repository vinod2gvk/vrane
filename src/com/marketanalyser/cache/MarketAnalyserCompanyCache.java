package com.marketanalyser.cache;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.CacheAccess;
import org.apache.commons.jcs.access.exception.CacheException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marketanalyser.model.Company;
import com.marketanalyser.service.ImportService;

@Service("companyCache")
public class MarketAnalyserCompanyCache {

	@Autowired
	private ImportService importService;
	
	
	public CacheAccess<String, HashMap<String,Company>> getCompanyCache(){
		 CacheAccess<String, HashMap<String,Company>> cache = null;
	        
	        try
	        {
	             cache = JCS.getInstance( "marketanalysercompanycache" );
	        }
	        catch ( CacheException e )
	        {
	               System.out.println("Cache is not initialised!!!");
	        }
	        
	        if(null == cache || null == cache.get("companiesMap") || cache.get("companiesMap").isEmpty() ) {
	        		cache.put("companiesMap", importService.readCompaniesFromCsv());
	        }
	        
	        return cache;
	}
}
