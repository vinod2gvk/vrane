package com.marketanalyser.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marketanalyser.model.CompanyDetails;
import com.marketanalyser.repository.CompanyDetailsDao;

/**
 * @author vinod
 *
 */
@Service("companyDetailsService")

public class CompanyDetailsServiceImpl implements CompanyDetailsService {

	@Autowired
	private CompanyDetailsDao companyDetailsDao;
	
	@Override
	public void addCompanyDetails(CompanyDetails companyDetails) {
		companyDetailsDao.addCompanyDetails(companyDetails);
		
	}

	@Override
	public List<CompanyDetails> listCompaninyDetails() {
		return companyDetailsDao.listCompanyDetails();
	}

	@Override
	public CompanyDetails getCompanyDetails(String companyId) {
		return companyDetailsDao.findCode(companyId);
	}

	@Override
	public void deleteCompanyDetails(CompanyDetails companyDetails) {
		companyDetailsDao.deleteCompanyDetails(companyDetails);
		
	}

	@Override
	public List<CompanyDetails> listCompaninyDetails(int pricePercentage, int minPERatio, int maxPERatio) {
		return companyDetailsDao.listCompanyDetails(pricePercentage,minPERatio,maxPERatio);
	}
	
	

}
