package com.marketanalyser.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.CacheAccess;
import org.apache.commons.jcs.access.exception.CacheException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.marketanalyser.cache.MarketAnalyserCompanyCache;
import com.marketanalyser.cache.MarketAnalyserCompanyDetailsCache;
import com.marketanalyser.controller.CompanyDetailsController;
import com.marketanalyser.model.Company;
import com.marketanalyser.model.CompanyDetails;

@Service("importService")
public class ImportService {

	@Autowired
	private CompanyService companyService;
	

	public static final Logger log = Logger.getLogger(ImportService.class);

	public HashMap<String,Company> readCompaniesFromCsv() {
		//String csvFile = "/Users/vinod/Downloads/companies.csv";
        String line = "";
        String cvsSplitBy = ",";

        HashMap<String,Company> listOfCompanies = new HashMap<>();
        ClassLoader classLoader = getClass().getClassLoader();
    	File file = new File(classLoader.getResource("companies.csv").getFile());


        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            while ((line = br.readLine()) != null) {

                String[] companyData = line.split(cvsSplitBy);

                Company company = new Company();
                
                String companyCode = companyData[0].replace("\"", "");
                String companyName = companyData[2].replace("\"", "");
                company.setCompanyCode(companyCode);
                company.setCompanyName(companyName);
                listOfCompanies.put(companyCode,company);
                
            }

           
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfCompanies;
	}
	
	public HashMap<String, CompanyDetails> importCompanyDetails() {
		List<Company> companies = companyService.listCompanies();
		HashMap<String,CompanyDetails> listOfCompanyDetails = new HashMap<>();
		for(Company company:companies) {
			
			try {
			
			Document doc = Jsoup.connect("http://money.rediff.com/companies/"+company.getCompanyCode()).get();
			Element currentPriceElement = doc.select("#ltpid").first();			
			Elements details = doc.select("#div_rcard_more .floatR");
			
			if(!CollectionUtils.isEmpty(details)) {
			String peRatio = details.get(0).text();
			String eps = details.get(1).text();
			String sales = details.get(2).text();
			String faceValue = details.get(3).text();
			String lastBonus = details.get(5).text();
			String lastDividend = details.get(6).text();

			CompanyDetails companyDetails = new CompanyDetails();
			
			companyDetails.setCode(company.getCompanyCode());
			
			if(null != currentPriceElement && StringUtils.isNotEmpty(currentPriceElement.text())) {
			companyDetails.setCurrentValue(new Double(currentPriceElement.text()));
			}
			
			if(StringUtils.isNotEmpty(peRatio)) {
				peRatio = peRatio.replace(",","");
				companyDetails.setPeRatio(new Double(peRatio));
			}
			if(StringUtils.isNotEmpty(eps)) {
				eps = eps.replace(",","");
			companyDetails.setEps(new Double(eps));
			}
			if(StringUtils.isNotEmpty(sales)) {
				sales = sales.replace(",","");
			companyDetails.setSales(new Double(sales));
			}
			if(StringUtils.isNotEmpty(faceValue)){
			companyDetails.setFaceValue(new Double(faceValue));
			}
			companyDetails.setLastBonus(lastBonus);
			
			if(StringUtils.isNotEmpty(lastDividend)) {
				lastDividend = lastDividend.replace(",","");
			companyDetails.setLastDivided(new Double(lastDividend));
			}
			
			listOfCompanyDetails.put(company.getCompanyCode(),companyDetails);
			}
		
			
			
			}catch(Exception e) {
				log.debug("Exception occured for company : "+company.getCompanyCode()+":"+company.getCompanyName()+" excepiton is : "+e.getMessage());
			}
			
		}
		return listOfCompanyDetails;
	}
}
